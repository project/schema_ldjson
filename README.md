# INTRODUCTION
------------
The module is easy to install/configure and easy to use.

Once you enable/install the module, it allows to store 3 types of schema codes i.e. Sitelink Schema, Product Schema & Local Business Schema. The module also provide the REST api to fetch all schema codes added through Drupal admin area. The module is basically used to add schema code on website which helps in terms of SEO.

The module is tested with PHP (v7.4) and Drupal 8.

# MAINTAINERS
-----------

Current maintainers:
 * Saumil Nagariya <saumil.nagariya@gmail.com>

This project has been sponsored by:
 * N/A
